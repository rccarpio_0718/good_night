class SleepSerializer < ActiveModel::Serializer
  attributes :id, :sleep_at, :wakeup_at
end

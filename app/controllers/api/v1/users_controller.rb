class Api::V1::UsersController < ApplicationController
    before_action :set_user, only: [:show, :clock_in, :follows, :sleep_records]

    def index
        render :json => {message: "Welcome to Good Night Application!"}
    end

    # POST /api/v1/users
    def create

    end

    # PUT /api/v1/users/{id}
    def update
        
    end

    def show
        render :json => @user
    end

    # ---- Custom methods

    # POST /api/v1/users/clock_in?id={user_id}
    def clock_in
        # Clock mode is a required params
        # Validation
        # - You must request first a sleep mode before a wake up mode otherwise will return an error response
        # - params: string clock_mode S: sleep mode W: wakeup mode

        render :json => {error: "Clock mode is a required parameter"} and return if params[:clock_mode].nil?
        render :json => {error: "Unknown clock mode parameter"} and return if !["S","W"].include?(params[:clock_mode].to_s)
        
        begin
            mode = params[:clock_mode].to_s.upcase
            mode_text = ""
            if mode == "S"
                mode_text = "Night"
                Sleep.set_sleep(@user)
            elsif mode == "W"
                mode_text = "Morning"
                sleep_mode = @user.sleeps.last
                if sleep_mode.nil?
                    render :json => {error: "Sleep mode not defined!"}
                    return
                end
                Sleep.set_wakeup(@user)
            end
            data = []
            clock_in = Sleep.where(user_id: @user.id).order(:created_at)
            clock_in.each do |c|
                data << {sleep_at: parse_date(c.sleep_at), wakeup_at: parse_date(c.wakeup_at), created_at: parse_date(c.created_at)}
            end
            render :json => {data: data}
        rescue StandardError
            render :json => {error: "#{$!}"}
        end
    end

    # POST /api/v1/users/follows?id={user_id}
    def follows
        # params: int follow_user_id - a user id that user want to follow
        # params: int type - a type of follow or unfollow, 1 > follow | -1 >unfollow

        render :json => {error: "Following user id is a required parameter"} and return if params[:follow_user_id].nil?
        render :json => {error: "Type is a required parameter"} and return if params[:type].nil?
        render :json => {error: "Unknown type parameter - #{params[:type]}"} and return if ![1,-1].include?(params[:type])

        friend_user = User.find(params[:follow_user_id].to_i) rescue nil
        render :json => {error: "User friend id not found"} and return if friend_user.nil?

        render :json => {error: "Invalid request!"} and return if friend_user.id == @user.id 

        begin
            Follower.set_follow(params, @user)
            msg = params[:type].to_i == 1 ? "follow" : "unfollow" 
            render :json => {message: "Successfully #{msg} the user @#{friend_user.name}."}
        rescue StandardError => e
            render :json => {error: "#{$!}"}
        end
    end

    def sleep_records
        begin
            sql = "
                select
                uf.id,
                uf.name,
                sum(s.duration_in_minute) as total_duration
                from followers f
                left join sleeps s on s.user_id = f.friends_id
                left join users uf on uf.id = f.friends_id
                where f.user_id = #{@user.id} and s.created_at >= '#{2.week.ago}' and s.created_at <= '#{1.week.ago}' 
                group by
                uf.name
                order by total_duration"
            results = ActiveRecord::Base.connection.exec_query( sql )
            data = []
            results.each do |d|
                sleep_data = Sleep.where(user_id: d['id'].to_i) || []
                sleep_data = sleep_data.map { |s| {sleep_at: parse_date(s.sleep_at), wakeup_at: parse_date(s.wakeup_at)} }
                data << {friend_name: d['name'], clock_in: sleep_data}
            end

            render :json => {data: data}
        rescue StandardError => e
            render :json => {error: "#{$!}"}
        end
    end

    private

        def set_user
            @user = User.find(params[:id])
        end

        def user_params
            params.require(:reservation).permit(:name)
        end

end
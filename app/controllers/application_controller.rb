class ApplicationController < ActionController::API
    def parse_date(date)
        DateTime.parse(date.to_s).strftime("%Y-%m-%d %I:%M:%S") rescue nil
    end
end
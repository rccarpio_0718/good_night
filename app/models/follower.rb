class Follower < ApplicationRecord
    belongs_to :user    

    def self.set_follow(params, current_user)
        follow = self.where(user_id: current_user.id, friends_id: params[:follow_user_id].to_i).first
        do_desert(params, follow, current_user)
    end

    # Insert or Destroy
    def self.do_desert(params, follow, current_user)
        self.create(user_id: current_user.id, friends_id: params[:follow_user_id].to_i) if params[:type].to_i == 1 && follow.nil?
        follow.destroy if params[:type].to_i == -1 && !follow.nil?
    end

end

class Sleep < ApplicationRecord
    belongs_to :user

    before_save :set_durations, if: ->() { wakeup_at.present? }

    def self.set_sleep(current_user)
        mode = self.where(user_id: current_user.id).last
        do_insert(current_user) if mode.nil?
        do_insert(current_user) if !mode.nil? && !mode.wakeup_at.nil?
    end

    def self.do_insert(current_user)
        self.create(user_id: current_user.id, sleep_at: DateTime.current)
    end

    def self.set_wakeup(current_user)
        mode = self.where(user_id: current_user.id).last
        mode.update(wakeup_at: DateTime.current) if !mode.nil? && mode.wakeup_at.nil?
    end

    def set_durations
        start_time = self.updated_at
        duration = Time.now - start_time
        in_minutes = duration / 60
        self.duration_in_minute = in_minutes.round(2)
    end

end

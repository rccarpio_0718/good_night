# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...

#--------------------------------------------------#
#--------------------------------------------------#

*** API Good Night by Rogelio Carpio ***

ruby version: 2.5.8
rails version: 6.1.6

* git clone https://gitlab.com/rccarpio_0718/good_night.git

* cd good_night

* bundle install

* Setup database
    - host: <localhost>
    - username: <root>
    - password: <password>

* Rake
    - rake db:drop db:create db:migrate db:seed

#--------------------------------------------------#
#--------------------------------------------------#

* API Endpoints

* POST Clockin

    > URL: http://localhost:3000/api/v1/users/clock_in?id=<current_user_id>

    > Sample request params:
        {
        "clock_mode": "S"
        }
    
        > "S" indicates Sleep

        > "W" indicates Wakeup

* POST Follow/Unfollow friends

    > URL: http://localhost:3000/api/v1/users/follows?id=<current_user_id>

    > Sample request params:
        {
        "follow_user_id": 1,
        "type": 1
        }

    > "follow_user_id" represents the friend's ID

    > "type" can be 1 for follow or -1 for unfollow

* GET Sleep records

    > URL: http://localhost:3000/api/v1/users/sleep_records?id=<current_user_id>

    > Please note that you should replace <current user id> with the actual ID of the current user.

* Seeds
        
    > There are 7 initial user records in the seeds.
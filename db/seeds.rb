# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create([
    {name: "Rogelio Carpio"},
    {name: "Anabelle Magano"},
    {name: "Aiden Timothy Carpio"},
    {name: "Aviel Carpio"},
    {name: "Guest 1"},
    {name: "Guest 2"},
    {name: "Guest 3"},
])
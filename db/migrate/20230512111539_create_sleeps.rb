class CreateSleeps < ActiveRecord::Migration[6.1]
  def change
    create_table :sleeps do |t|
      t.integer :user_id
      t.datetime :sleep_at
      t.datetime :wakeup_at
      t.string :duration_in_minute
      t.timestamps
    end
  end
end